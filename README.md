# Pong-Networks

## Contibuters
- Carson Lance (clance1)
- Cole Pickford (cpickfor)
- Jack Conway (jconway7)

## Usage
Simply run make and then run `./pong --host PORT` to establish the server and `./pong ADDRESS PORT` to setup the client. In order to move the paddles use the up/down arrows for the client and the w/s keys for the server.

## Errata
We were having issues with the speed of the program on the student machines. We are unsure whether it roots from our code or the amount of users on the student machines. We were also unable to transmit any messages across any student machines beside student00 and student01.