/*********************
*
* pong_net.cpp
* Carson Lance (clance1)
* Cole Pickford (cpickfor)
* Jack Conway (jconway7)
*
***********************/

#include <ncurses.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <string.h>
#include <sys/time.h>
#include <netdb.h>
#include <signal.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <errno.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <sys/types.h>

#define WIDTH 43
#define HEIGHT 21
#define PADLX 1
#define PADRX WIDTH - 2

// Global variables recording the state of the game
// Position of ball
int ballX, ballY;
// Movement of ball
int dx, dy;
// Position of paddles
int padLY, padRY;
// Player scores
int scoreL, scoreR;

// ncurses window
WINDOW *win;

/* Draw the current game state to the screen
 * ballX: X position of the ball
 * ballY: Y position of the ball
 * padLY: Y position of the left paddle
 * padRY: Y position of the right paddle
 * scoreL: Score of the left player
 * scoreR: Score of the right player
 */
void draw(int ballX, int ballY, int padLY, int padRY, int scoreL, int scoreR) {
    // Center line
    int y;
    for(y = 1; y < HEIGHT-1; y++) {
        mvwaddch(win, y, WIDTH / 2, ACS_VLINE);
    }
    // Score
    mvwprintw(win, 1, WIDTH / 2 - 3, "%2d", scoreL);
    mvwprintw(win, 1, WIDTH / 2 + 2, "%d", scoreR);
    // Ball
    mvwaddch(win, ballY, ballX, ACS_BLOCK);
    // Left paddle
    for(y = 1; y < HEIGHT - 1; y++) {
        int ch = (y >= padLY - 2 && y <= padLY + 2)? ACS_BLOCK : ' ';
        mvwaddch(win, y, PADLX, ch);
    }
    // Right paddle
    for(y = 1; y < HEIGHT - 1; y++) {
        int ch = (y >= padRY - 2 && y <= padRY + 2)? ACS_BLOCK : ' ';
        mvwaddch(win, y, PADRX, ch);
    }
    // Print the virtual window (win) to the screen
    wrefresh(win);
    // Finally erase ball for next time (allows ball to move before next refresh)
    mvwaddch(win, ballY, ballX, ' ');
}

/* Return ball and paddles to starting positions
 * Horizontal direction of the ball is randomized
 */
void reset() {
    ballX = WIDTH / 2;
    padLY = padRY = ballY = HEIGHT / 2;
    // dx is randomly either -1 or 1
    dx = (rand() % 2) * 2 - 1;
    dy = 0;
    // Draw to reset everything visually
    draw(ballX, ballY, padLY, padRY, scoreL, scoreR);
}

/* Display a message with a 3 second countdown
 * This method blocks for the duration of the countdown
 * message: The text to display during the countdown
 */
void countdown(const char *message) {
    int h = 4;
    int w = strlen(message) + 4;
    WINDOW *popup = newwin(h, w, (LINES - h) / 2, (COLS - w) / 2);
    box(popup, 0, 0);
    mvwprintw(popup, 1, 2, message);
    int countdown;
    for(countdown = 3; countdown > 0; countdown--) {
        mvwprintw(popup, 2, w / 2, "%d", countdown);
        wrefresh(popup);
        sleep(1);
    }
    wclear(popup);
    wrefresh(popup);
    delwin(popup);
    padLY = padRY = HEIGHT / 2; // Wipe out any input that accumulated during the delay
}

/* Perform periodic game functions:
 * 1. Move the ball
 * 2. Detect collisions
 * 3. Detect scored points and react accordingly
 * 4. Draw updated game state to the screen
 */
void tock() {
    // Move the ball
    ballX += dx;
    ballY += dy;

    // Check for paddle collisions
    // padY is y value of closest paddle to ball
    int padY = (ballX < WIDTH / 2) ? padLY : padRY;
    /*

    */
    // colX is x value of ball for a paddle collision
    int colX = (ballX < WIDTH / 2) ? PADLX + 1 : PADRX - 1;
    if(ballX == colX && abs(ballY - padY) <= 2) {
        // Collision detected!
        dx *= -1;
        // Determine bounce angle
        if(ballY < padY) dy = -1;
        else if(ballY > padY) dy = 1;
        else dy = 0;
    }

    // Check for top/bottom boundary collisions
    if(ballY == 1) dy = 1;
    else if(ballY == HEIGHT - 2) dy = -1;

    // Have the ball be handled by whatever side the ball is on
    // Also the score
    // Score points
    if(ballX == 0) {
        scoreR = (scoreR + 1) % 100;
        reset();
        countdown("SCORE -->");
    } else if(ballX == WIDTH - 1) {
        scoreL = (scoreL + 1) % 100;
        reset();
        countdown("<-- SCORE");
    }

    // Finally, redraw the current state
    /*
    */
    draw(ballX, ballY, padLY, padRY, scoreL, scoreR);

}

/* Listen to keyboard input
 * Updates global pad positions
 */
void *listenInput(void *args) {
    while(1) {
        switch(getch()) {
            case KEY_UP: padRY--;
             break;
            case KEY_DOWN: padRY++;
             break;
            case 'w': padLY--;
             break;
            case 's': padLY++;
             break;
            default: break;
       }
    }
    return NULL;
}

void debug(const char *message) {
  int h = 4;
  int w = strlen(message) + 4;
  WINDOW *popup = newwin(h, w, (LINES - h) / 2, (COLS - w) / 2);
  box(popup, 0, 0);
  mvwprintw(popup, 1, 2, message);
  sleep(1);
  wclear(popup);
  wrefresh(popup);
  delwin(popup);
}

void initNcurses() {
    initscr();
    cbreak();
    noecho();
    keypad(stdscr, TRUE);
    curs_set(0);
    refresh();
    win = newwin(HEIGHT, WIDTH, (LINES - HEIGHT) / 2, (COLS - WIDTH) / 2);
    box(win, 0, 0);
    mvwaddch(win, 0, WIDTH / 2, ACS_TTEE);
    mvwaddch(win, HEIGHT-1, WIDTH / 2, ACS_BTEE);
}

void handler(int signal) {
    // do cleanup tasks (send termination message to peer, close socket cleanly, etc.)
    endwin(); // clean up ncurses
    exit(0);
}

int main(int argc, char *argv[]) {
    int sockfd , client_sock , c;
    struct sockaddr_in server , client;
    int addr_len;
    char* hostname;
    int len;
    int n = -1;

    bool host = false;
    int port = 0;
    // Process args
    if (argc < 2) {
      printf("USAGE: ./pong_net [--host | HOSTNAME] PORT\n\tOnly use --host flag for one\n");
    }
    else if (strcmp(argv[1], "--host") == 0) {
      host = true;
      port = atoi(argv[2]);
    }
    else {
      hostname = argv[1];
      port = atoi(argv[2]);
      printf("Hostname: %s\nPort: %d\n", hostname, port);
    }

    //Create socket
    sockfd = socket(PF_INET, SOCK_DGRAM, 0);
    if (sockfd == -1) {
        printf("Could not create socket");
    }

    if (host) {

      // refresh is clock rate in microseconds
      // This corresponds to the movement speed of the ball
      int refresh;
      char difficulty[10];
      printf("Please select the difficulty level (easy, medium or hard): ");
      scanf("%s", &difficulty);
      if(strcmp(difficulty, "easy") == 0) refresh = 80000;
      else if(strcmp(difficulty, "medium") == 0) refresh = 40000;
      else if(strcmp(difficulty, "hard") == 0) refresh = 20000;
      memset(&server, 0, sizeof(server));
      memset(&client, 0, sizeof(client));


      //Prepare the sockaddr_in structure
      server.sin_family = AF_INET;
      server.sin_addr.s_addr = INADDR_ANY;
      server.sin_port = htons(port);

      //Bind
      if(bind(sockfd,(struct sockaddr *)&server , sizeof(server)) < 0) {
         //print the error message
         perror("bind failed. Error");
         return 1;
      }

      char buffer[BUFSIZ];
      printf("Wating for connection on port %d\n", port);
      addr_len = sizeof(client);
      while (n < 0) {
        n = recvfrom(sockfd, buffer, sizeof(buffer), 0, ( struct sockaddr *) &client, (socklen_t *)&addr_len);
      }
    }
    else {
    	memset(&server, 0, sizeof(server));

      struct hostent * hp = gethostbyname(argv[1]);
      if (!hp) {
        fprintf(stderr, "no host");
      }
      bcopy(hp->h_addr, (char*)&server.sin_addr, hp->h_length);

    	server.sin_family = AF_INET;
    	server.sin_port = htons(port);

      char *hello = "Hello from client";
      char buffer[BUFSIZ];
      int t = sendto(sockfd, (const char *)hello, strlen(hello), 0, (struct sockaddr *) &server, sizeof(struct sockaddr_in));
      if (t < 0) {
        fprintf(stderr, "ERROR: %s\n", strerror(errno));
      }
    }


    // Set up ncurses environment
    initNcurses();

    // Set starting game state and display a countdown
    reset();
    countdown("Starting Game");

    // Listen to keyboard input in a background thread
    pthread_t pth;
    pthread_create(&pth, NULL, listenInput, NULL);

    // Main game loop executes tock() method every REFRESH microseconds
    struct timeval tv;
    char tempRY[BUFSIZ], tempLY[BUFSIZ], tempBX[BUFSIZ], tempBY[BUFSIZ], tempSR[BUFSIZ], tempSL[BUFSIZ];
    while(1) {
        gettimeofday(&tv,NULL);
        unsigned long before = 1000000 * tv.tv_sec + tv.tv_usec;
        if (host) {
          tock(); // Update game state
          memset(&client, 0, sizeof(client));
          n = recvfrom(sockfd, tempRY, sizeof(tempRY), 0, (struct sockaddr *) &client, (socklen_t *)&addr_len);
          padRY = atoi(tempRY);


          //if (ballX <= WIDTH / 2) {
            sprintf(tempBX, "%d", ballX);
            sprintf(tempBY, "%d", ballY);

            sendto(sockfd, tempBX, sizeof(tempBX), 0, (const struct sockaddr *) &client, sizeof(struct sockaddr_in));
            sendto(sockfd, tempBY, sizeof(tempBY), 0, (const struct sockaddr *) &client, sizeof(struct sockaddr_in));
            /*
          }
          else {
            n = recvfrom(sockfd, tempBX, sizeof(tempBX), 0, ( struct sockaddr *) &client, (socklen_t *)&addr_len);
            n = recvfrom(sockfd, tempBY, sizeof(tempBY), 0, ( struct sockaddr *) &client, (socklen_t *)&addr_len);
            ballX = atoi(tempBX);
            ballY = atoi(tempBY);
          }
          */

          sprintf(tempLY, "%d", padLY);
          sprintf(tempSL, "%d", scoreL);
          sendto(sockfd, tempLY, sizeof(tempLY), 0, (const struct sockaddr *) &client, sizeof(struct sockaddr_in));
          sendto(sockfd, tempSL, sizeof(tempSL), 0, (const struct sockaddr *) &client, sizeof(struct sockaddr_in));

          draw(ballX, ballY, padLY, padRY, scoreL, scoreR);
        }
        else {
          int addr_len = sizeof(server);
          sprintf(tempRY, "%d", padRY);
          sendto(sockfd, tempRY, sizeof(int), 0, (struct sockaddr *)&server, sizeof(struct sockaddr_in));

          /*
          if (ballX > WIDTH / 2) {
            sprintf(tempBX, "%d", ballX);
            sprintf(tempBY, "%d", ballY);

            sendto(sockfd, tempBX, sizeof(tempBX), 0, (const struct sockaddr *) &server, sizeof(struct sockaddr_in));
            sendto(sockfd, tempBY, sizeof(tempBY), 0, (const struct sockaddr *) &server, sizeof(struct sockaddr_in));
          }
          else {
            */
            n = recvfrom(sockfd, tempBX, sizeof(tempBX), 0, ( struct sockaddr *) &server, (socklen_t *)&addr_len);
            n = recvfrom(sockfd, tempBY, sizeof(tempBY), 0, ( struct sockaddr *) &server, (socklen_t *)&addr_len);
            ballX = atoi(tempBX);
            ballY = atoi(tempBY);
        //  }

          n = recvfrom(sockfd, tempLY, sizeof(tempLY), 0, ( struct sockaddr *) &server, (socklen_t *)&addr_len);
          n = recvfrom(sockfd, tempSL, sizeof(tempSL), 0, ( struct sockaddr *) &server, (socklen_t *)&addr_len);
          padLY = atoi(tempLY);
          scoreL = atoi(tempSL);

          // Score points
          if(ballX == 0) {
              scoreR = (scoreR + 1) % 100;
              reset();
              countdown("SCORE -->");
          } else if(ballX == WIDTH - 1) {
              reset();
              countdown("<-- SCORE");
          }

          draw(ballX, ballY, padLY, padRY, scoreL, scoreR);
        }
        gettimeofday(&tv,NULL);
        unsigned long after = 1000000 * tv.tv_sec + tv.tv_usec;
        unsigned long toSleep = refresh - (after - before);
        // toSleep can sometimes be > refresh, e.g. countdown() is called during tock()
        // In that case it's MUCH bigger because of overflow!
        if(toSleep > refresh) toSleep = refresh;
        usleep(toSleep); // Sleep exactly as much as is necessary

    }


    // Clean up
    signal(SIGINT, handler);
    pthread_join(pth, NULL);
    endwin();

    return 0;
}
